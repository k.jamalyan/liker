import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/app/App';
import 'bootstrap/dist/css/bootstrap.min.css';
import * as serviceWorker from './serviceWorker';
import * as firebase from "firebase";



const firebaseConfig = {
    apiKey: "**********************************",
    authDomain: "*****************************",
    databaseURL: "*****************************",
    projectId: "**************************",
    storageBucket: "*********************",
    messagingSenderId: "*************",
    appId: "**********************************"
};
if(!firebase.apps.length){
    firebase.initializeApp(firebaseConfig);
}


ReactDOM.render(
    <App />,
  document.getElementById('root')
);

serviceWorker.unregister();
