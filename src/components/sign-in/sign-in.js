import React, {Component} from "react";
import "./sign-in.css";
import {Form, Button} from "react-bootstrap"
import firebase from "firebase";

class SignIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            error: '',
        };
    };
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    login = (e) => {
        const {email,password} = this.state
        e.preventDefault();
        firebase.auth().signInWithEmailAndPassword(email,password).then((u)=>{
            window.location = '/'
        }).catch((error) => {
            this.setState({
      // poxel mesij@
                error: " error"
            })

        });
    }
    render() {

        return (
            <div className="container-fluid bgIn">
                <div className="row">
                    <div className="col-md-4 col-sm-4 col-xs-12"></div>
                    <div className="col-md-4 col-sm-4 col-xs-12">
                        {/*form start*/}
                        <Form className="form-conatiner">
                            <h1>Sign In</h1>
                            <Form.Group>
                                <Form.Label > Email </Form.Label>
                                <Form.Control  onChange={this.handleChange}   type="email" name="email" placeholder="Enter email" />
                            </Form.Group>
                            <Form.Group>
                                <Form.Label  > Password </Form.Label>
                                <Form.Control  onChange={this.handleChange} type="password" name="password"  placeholder="Enter Password" />
                            </Form.Group>
                            <Form.Group controlId="formBasicCheckbox">
                                <Form.Check type="checkbox" label="Check me out" />
                            </Form.Group>
                            {this.state.error && <p style={{color:'red'}}>{this.state.error}</p>}
                            <Button onClick={this.login} variant="success" className="btn-block" type="submit">
                                Submit
                            </Button>
                        </Form>
                        {/*form end  */}
                    </div>
                    <div className="col-md-4 col-sm-4 col-xs-12"></div>
                </div>
            </div>
    )
    }
    }
    export default SignIn;