import React, {Component} from "react";
import "./sign-up.css";
import {Form, Button} from "react-bootstrap"
import firebase from "firebase"


class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
        error:''
        }

    }

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    signup = (e) => {

        const tath = this
        const {email,password,username}=this.state;

        e.preventDefault();
        if(email && password && username){

        firebase.auth().createUserWithEmailAndPassword(email, password).then((u)=>{
            firebase.auth().currentUser.updateProfile({
                displayName: username,

            }).then(function() {
                window.location = "/";
            }).catch(function(error) {

                tath.setState({
                    error:error.message
                })
            });
        })
            .catch((error) => {

               this.setState({
                   error:error.message
               })
            })


        }else {
            this.setState({
                error:'Input is empty'
            })
        }

    }

    render() {
        return (
            <div className="container-fluid bgUp">
                <div className="row">
                    <div className="col-md-4 col-sm-4 col-xs-12"> </div>
                    <div className="col-md-4 col-sm-4 col-xs-12">

                        {/*form start*/}
                        <Form className="form-container-up">
                            <h1>Sign Up</h1>
                            <Form.Group>
                                <Form.Label > User Name</Form.Label>
                                <Form.Control  onChange={this.handleChange}   type="text" name="username" placeholder="user name" />
                            </Form.Group>
                            <Form.Group>
                                <Form.Label > Email </Form.Label>
                                <Form.Control   onChange={this.handleChange} type="email" name="email" placeholder="email" />
                            </Form.Group>
                            <Form.Group>
                                <Form.Label  > Password </Form.Label>
                                <Form.Control  onChange={this.handleChange} type="password" name="password"  placeholder="password" />
                            </Form.Group>
                            {this.state.error && <p style={{color:'red'}}>{this.state.error}</p>}

                            <Button onClick={this.signup} variant="success" className="btn-block" type="submit">
                                Submit
                            </Button>
                        </Form>


                        {/*form end  */}
                    </div>
                    <div className="col-md-4 col-sm-4 col-xs-12"></div>
                </div>
            </div>
        )
    }
}
export default SignUp;