import React,{useState}  from "react";
import {Carousel} from "react-bootstrap";
import wallpaper1 from "../../assets/images/wallpaper1.jpg";
import wallpaper2 from "../../assets/images/wallpaper2.jpg";
import "./slider.css"



const Slider = () => {
    const [index, setIndex] = useState(0);
    const handleSelect = (selectedIndex, e) => {
        setIndex(selectedIndex);
    };
    return (
        <div className="slider">
            <Carousel   activeIndex={index} onSelect={handleSelect}>
                <Carousel.Item >
                    <img
                        className="d-block w-100"
                        src={wallpaper1}
                        alt="First slide"
                    />
                    <Carousel.Caption >
                        <h3>First slide label</h3>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={wallpaper2}
                        alt="Second slide"
                    />

                    <Carousel.Caption>
                        <h3>Second slide label</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={wallpaper1}
                        alt="Third slide"
                    />

                    <Carousel.Caption>
                        <h3>Third slide label</h3>
                        <p>
                            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
                        </p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        </div>
    );



}
export default Slider;