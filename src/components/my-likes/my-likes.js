import React, {Component} from "react";
import "./my-likes.css";
import firebase from "firebase";


class MyLikes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            user: {},
            arr: [],
            followed_v: [],
            videoStatus: false
        };


    };

    componentDidMount() {
        this.authListener();
        /////get firebase data
        let followed = [];
        const db = firebase.database()
        // const key= 0
        db.ref('/')
            .on('value', (elem) => {
                if (elem.val()) {
                    Object.keys(elem.val().videos).map(() => {
                        followed = elem.val().videos[this.state.user.uid]
                    })
                }
                if (followed) {
                    this.setState({
                        followed_v: followed,
                        arr: followed
                    })
                    if(followed.length === 0){
                        this.setState({
                            videoStatus: true
                        })
                    }
                }
            })


    }


    authListener = () => {

        firebase.auth().onAuthStateChanged((user) => {
            if (user) {

                this.setState({
                    user: user
                })

            } else {
                this.setState({
                    user: ''
                })
            }

        });
    }


    deleteData = (event) => {
        event.preventDefault()
        let arr = [];
        const {id} = event.target;
        const {followed_v} = this.state;


        followed_v.map((elem) => {
            if (elem.v_id !== id) {
                arr.push(elem)
            }
        })
        if(arr.length ===0){
            window.location.reload()
        }
        //set firebase
        firebase
            .database()
            .ref('/videos/' + this.state.user.uid)
            .set(arr);

    }


    render() {

         if (this.state.videoStatus === false){
        return (
            <div className = "container-fluid">
                <div className=" row myLikes justify-content-center"> My like</div>
                <div className="row">
                    {
                        this.state.arr.map((video, key) => {
                            return (
                                <div key={key} className=" col-12 col-md-6 col-lg-4 ">
                                    <div>
                                        <img src={video.v_url} alt="not img" className="videoItems "/>
                                    </div>
                                    <div className="likeVideosButton ">
                                        {(() => {
                                            if (this.state.user.uid != null && this.state.followed_v) {

                                                return (
                                                    <input
                                                        type="button"
                                                        className="btn btn-primary"
                                                        id={video.v_id}
                                                        onClick={this.deleteData}
                                                        value="Unfollow"
                                                    />
                                                );
                                            }
                                        })()}
                                    </div>
                                </div>


                            )
                        })

                    }
                </div>

                }
                }
            </div>
        )
         }else if (this.state.videoStatus === true){
             return (
                 <div className="container-fluid">
                 <div className=" row myLikes justify-content-center"> My like</div>
                 <p className="noVideo"> You have not liked videos</p>
                 </div>
             )
         }
    }
}

export default MyLikes;