import React, {Component} from "react";
import "./profile.css";
import firebase from "firebase";
import {Button,Form,Modal} from "react-bootstrap";


class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            errorPassword: '',
            errorEmail: '',
            errorName: '',
            errorDelete: '',
            showModal: false,
        }

    }


    handleChange = (e) => {


        this.setState({[e.target.name]: e.target.value.trim()});

    };

    changeName = (e) => {
        let that = this;
        e.preventDefault();
        let user = firebase.auth().currentUser
        const {value} = this.state;
        if (value.length < 3) {
            this.setState({
                errorName: "Atleast 3 characaters required"

            })
        } else {

            user.updateProfile({
                displayName: value,
            }).then(function () {
                window.location.reload();
            }).catch(function (error) {
                that.setState({
                    errorName: error.message
                })
            });
        }
    };
    changeEmail = (e) => {
        let that = this;
        e.preventDefault();
        let user = firebase.auth().currentUser;
        const {newEmail} = this.state
        user.updateEmail(newEmail).then(function () {
            window.location.reload();
        }).catch(function (error) {
            that.setState({
                errorEmail: error.message
            })
        });
    }


    changePassword = (e) => {
        let that = this;
        e.preventDefault();
        let user = firebase.auth().currentUser;
        const {newPassword} = this.state;
        user.updatePassword(newPassword).then(function () {
            window.location.reload();
        }).catch(function (error) {
            that.setState({
                errorPassword: error.message
            })
        });


    }

    deleteUser = (e) => {
        let that = this;
        e.preventDefault();
        let user = firebase.auth().currentUser;
        user.delete().then(function () {
            window.location = "/";
        }).catch(function (error) {
            that.setState({
                errorDelete: error.message
            })
        });
    }
  handelOpen= () =>{
        this.setState({
            showModal:true,
        })
  }
    handleClose = () =>{
        this.setState({
            showModal:false,
        })

}
    render() {

        return (
            <div >
                <div className="profile row justify-content-center">

                    <div className="col-12 col-lg-4 profileInfo">
                        <h3>Personal information</h3>
                        <br/>
                        <h4>Name: {firebase.auth().currentUser ? firebase.auth().currentUser.displayName : ''} </h4>
                        <h4>Your
                            email: {firebase.auth().currentUser ? firebase.auth().currentUser.email : ''}  </h4>
                    </div>

                    <div className="col-12 col-lg-4 profileChange " >



                        {/*New user name*/}
                        <Form>
                            <Form.Group>
                                <Form.Label> New Name</Form.Label>
                                <Form.Control onChange={this.handleChange} type="text" name="value"
                                              placeholder="New name"/>
                                {this.state.errorName && <strong>{this.state.errorName}</strong>}
                            </Form.Group>
                            <Button onClick={this.changeName} variant="success" className="btn" type="submit">
                                Change name
                            </Button>
                        </Form>


                        {/*New email*/}

                        <br/>
                        <Form>
                            <Form.Group>
                                <Form.Label> New email</Form.Label>
                                <Form.Control onChange={this.handleChange} type="text" name="newEmail"
                                              placeholder="New email"/>
                                {this.state.errorEmail && <strong>{this.state.errorEmail}</strong>}
                            </Form.Group>

                            <Button onClick={this.changeEmail} variant="success" className="btn" type="submit">
                                Change email
                            </Button>
                        </Form>


                        {/*New password*/}
                        <br/>
                        <Form>
                            <Form.Group>
                                <Form.Label> New password</Form.Label>
                                <Form.Control onChange={this.handleChange} type="text" name="newPassword"
                                              placeholder="New password"/>
                                {this.state.errorPassword && <strong>{this.state.errorPassword}</strong>}
                            </Form.Group>

                            <Button onClick={this.changePassword} variant="success" className="btn" type="submit">
                                Change password
                            </Button>

                        </Form>


                        {/*Delete account*/}
                        <br/>
                        <Form.Label> </Form.Label>
                        <Button onClick={this.handelOpen} variant="danger" className="btn" type="submit">Delete
                            account</Button>
                    </div>
                </div>
                <Modal show={this.state.showModal} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>When you press the delete button, the account will be deleted forever!</Modal.Title>
                    </Modal.Header>

                    <Modal.Footer>
                        <Button variant="danger" onClick={this.deleteUser}>
                            Delete
                        </Button>
                        <Button variant="secondary" onClick={this.handleClose}>
                            Close
                        </Button>
                        {this.state.errorDelete && <strong>{this.state.errorDelete}</strong>}

                    </Modal.Footer>
                </Modal>
            </div>
        )
    }


}

export default Profile;
