import React, {Component} from 'react';
import './App.css';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom"
import firebase from "firebase";
import Header from "../header/header";
import Home from "../home/home"
import SignIn from "../sign-in/sign-in";
import SignUp from "../sign-up/sign-up";
import MyLikes from "../my-likes/my-likes";
import Profile from "../profile/profile";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            user: null,
            hasAccount: false,
        });
    }

    componentDidMount() {
        this.authListener();

    }

    authListener = () => {
        firebase.auth().onAuthStateChanged((user) => {

            if (user) {
                this.setState({
                    user,
                });
            } else {
                this.setState({
                    user: null,
                });
            }
        });
    }


    render() {


        return (
            <div >
                <Router>
                    <Header/>
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route path="/signin" component={SignIn}/>
                        <Route path="/signup" component={SignUp}/>
                        <Route exact path="/profile" component={Profile}/>
                        <Route path="/mylikes" component={MyLikes}/>
                    </Switch>
                </Router>
            </div>
        );


    }
}

export default App;
