import React from "react";
import "./video-list.css"
import firebase from "firebase";
import VideoModal from "../video-modal/video-modal";

class VideoList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            user: {},
            arr: [],
            followed_v: [],
            showModal: false,
            video: null,
        }
    }

    componentDidMount() {
        this.authListener();
        const YOUTUBE_API_KEY = "*************************************";
        let that = this;
        const url = `https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=UCa8roge8M79zg-bzT-rqp9g&maxResults=10&key=${YOUTUBE_API_KEY}`;
        fetch(url)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    videos: data.items,
                    statusLike: true,
                })

            })

        let followed = [];
        firebase
            .database()
            .ref('/')
            .on('value', function (snapshot) {
                if (snapshot.val()) {
                    Object.keys(snapshot.val().videos).map(() => {
                        followed = snapshot.val().videos[that.state.user.uid]
                    })
                }
                if (followed) {
                    that.setState({
                        followed_v: followed,
                        arr: followed

                    })
                }
            })
        ////////////////////////
    }


    authListener = () => {

        firebase.auth().onAuthStateChanged((user) => {
            if (user) {

                this.setState({
                    user: user
                })

            } else {
                this.setState({
                    user: ''
                })
            }

        });
    }


    sendData = (event) => {
        if (!this.state.user) {
            window.location.href = '/signin'
        }
        const {id} = event.target;
        const {arr} = this.state;
        const {img} =  event.target.dataset;

        if (this.state.user.uid) {
            arr.push({
                u_id: this.state.user.uid,
                v_id: id,
                v_url: img
            })


        }   // this.setState({
            //     arr: arr
            // })



        firebase
            .database()
            .ref('/videos/' + this.state.user.uid)
            .set(arr);
    }
//////// Modal functions //////////
    handleModalOpen = (video) => {
        this.setState({
            showModal: true,
            video,
        });
    };
    handleModalClose = () => {
        this.setState({
            showModal: false,
            video: null
        });
    };

    render() {
        return (
            <div className="container">
                <br/>
                {this.state.video && <VideoModal
                    showModal={this.state.showModal}
                    handleModalClose={this.handleModalClose}
                    video={this.state.video}
                />}
                <div className="row titleVideos justify-content-center"> Video for Youtube</div>
                <br/>
                <div className="row">
                {this.state.videos && this.state.videos.map((video, key) => {
                        return (
                            <div key={key} className="col-12 col-md-6 col-lg-4 col-xl-3 align-items-center
" >

                 <span variant="outline-danger" onClick={() => this.handleModalOpen(video)}>
                    <img className="videoItems" src={video.snippet.thumbnails.high.url} alt="video"/>
                    <h6 className="videoName">{video.snippet.title}</h6>
                </span>

                                    {(() => {
                                        if (this.state.user.uid != null && this.state.followed_v) {
                                            if (
                                                this.state.followed_v.find(
                                                    videos => videos.v_id === video.id.videoId
                                                )
                                            ) {
                                                return (
                                                    <input
                                                        type="button"
                                                        className="btn btn-primary"
                                                        id={video.id.videoId}
                                                        onClick={() => console.log('ka')}
                                                        value="Followed"
                                                        disabled="disabled"
                                                    />

                                                );
                                            } else {
                                                return (
                                                    <input
                                                        type="button"
                                                        className="btn btn-success"
                                                        id={video.id.videoId}
                                                        data-img = {video.snippet.thumbnails.high.url}
                                                        onClick={this.sendData}
                                                        value="Follow"
                                                    />
                                                );
                                            }
                                        } else {
                                            return (
                                                <input
                                                    type="button"
                                                    className="btn btn-success"
                                                    id={video.id.videoId}
                                                    data-img = {video.snippet.thumbnails.high.url}
                                                    onClick={this.sendData}
                                                    value="Follow"
                                                />
                                            );
                                        }
                                    })()}

                            </div>

                        )
                    }
                )}
                </div>
            </div>
        )
    }


}

export default VideoList;
