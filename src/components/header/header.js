import React, {Component} from "react";
import "./header.css"
import {Navbar, Nav, Dropdown, Button,} from "react-bootstrap";
import firebase from "firebase";
import {Link} from "react-router-dom";
import  profile_img from "../../assets/images/profil_img.jpg"
import logo from "../../assets/images/logo.png"
class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    logout = () => {
        firebase.auth().signOut();
    }

    render() {


        return (
            <div className="headerNavbar">
                <Navbar collapseOnSelect expand="lg" variant="dark">
                    <Navbar.Brand><Link to="/"><img className="logo" src={logo} alt="not logo"/></Link></Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="mr-auto">
                            {/*ես հանելուց  սաղ խառնվումա իրար*/}
                        </Nav>


                        <Nav>
                            {!firebase.auth().currentUser ?
                                <>
                                    <Nav.Link  href="/signup">Sign Up</Nav.Link>
                                    <Nav.Link href="/signin">Sign In</Nav.Link>
                                </>
                                :
                                <>
                                    <Dropdown >
                                        <Dropdown.Toggle variant="tr" id="dropdown-basic ">
                                            <img className="profilePhoto"
                                                 src={profile_img}
                                                 alt="not Img"/>
                                        </Dropdown.Toggle>
                                        <Dropdown.Menu >
                                            <Dropdown.Item href="/profile">Profile</Dropdown.Item>
                                            <Dropdown.Item href="/mylikes">My Like</Dropdown.Item>
                                            <Dropdown.Item><Button
                                                onClick={this.logout}
                                                variant="danger m-2"> Sign Out
                                            </Button>
                                            </Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </>

                            }
                        </Nav>

                    </Navbar.Collapse>
                </Navbar>
            </div>
        )
    }
}

export default Header;
