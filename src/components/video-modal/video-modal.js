import React from "react";
import "./video-modal.css";
import { Modal} from "react-bootstrap";




const VideoModal = ({video, showModal, handleModalClose}) => {


    return(
        <Modal

            show={showModal}
            onHide={handleModalClose}
            dialogClassName="modal-90w"
            aria-labelledby="example-custom-modal-styling-title"
        >
            <Modal.Header closeButton>
                <Modal.Title id="example-custom-modal-styling-title">
                    {video.snippet.title}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <iframe title="youtube Video" src={`https://www.youtube.com/embed/${video.id.videoId}`} width="100%" height="360px" />
            </Modal.Body>
        </Modal>

    )

};

export default VideoModal;